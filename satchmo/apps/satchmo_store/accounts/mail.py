"""Sends mail related to accounts."""
import string, datetime
from random import choice

from django.conf import settings
from django.utils.translation import ugettext
from django.contrib.sites.models import Site

from satchmo_store.mail import send_store_mail
from satchmo_store.shop.models import Config
from satchmo_store.shop.signals import registration_sender

from product.models import Discount

import logging
log = logging.getLogger('satchmo_store.accounts.mail')

def crea_sconto_benvenuto(contact):
    log.info("Creo un buono sconto di benvenuto per il contatto %s con relativo utente %s", contact.full_name,
             contact.user.username)
    d = Discount()
    d.site = Site.objects.get_current()
    d.description = 'Sconto Benvenuto - %s' % contact.full_name

    # Genero un codice alfanumerico random di sicurezza
    chars = string.letters + string.digits
    codice_sicurezza = ''.join([choice(chars) for i in range(4)])
    # il codice del buonosconto puo essere di max 20 caratteri.
    # Avendone 4 per la scritta benv, 2 per i trattini e 4 per il codice
    # di sicurezza fermo lo username a max 10 caratteri
    d.code = 'BENV-%s-%s' % (contact.user.username[:10].upper(), codice_sicurezza.upper())

    d.description = 'Sconto Benvenuto - %s' % contact.full_name
    d.active = True
    d.amount = settings.BUONOSCONTO_BENVENUTO_AMOUNT
    d.allowedUses = settings.BUONOSCONTO_BENVENUTO_ALLOWED_USES
    d.minOrder = settings.BUONOSCONTO_BENVENUTO_MIN_ORDER
    d.startDate = datetime.date.today()
    d.endDate = datetime.date.today() + datetime.timedelta(days=90)
    d.allValid = True
    d.save()
    return d


# TODO add html email template
def send_welcome_email(contact, first_name, last_name):
    """Send a store new account welcome mail to `email`."""

    # Creo lo sconto di benvenuto.
    sconto = None
    if settings.ENABLE_SCONTO_BENVENUTO:
        sconto = crea_sconto_benvenuto(contact)

    if settings.ENABLE_MANDRILL:
        from ex_satchmo.tools.mandrill import MandrillBookrepublic
        MandrillBookrepublic.send_welcome_mail(contact=contact, discount_code=sconto.code)

    elif settings.ENABLE_ECIRCLE:
        from ex_satchmo.tools.ecircle import Ecircle

        ec = Ecircle()
        ec.send_transactional_mail(settings.ECIRCLE_GROUP_ID['bookrepublic'], contact.email, first_name, last_name,
                                   settings.ECIRCLE_MESSAGE_ID['benvenuto'], ['codice_sconto'], [sconto.code])
        ec.logout()
    else:
        shop_config = Config.objects.get_current()
        c = {
            'first_name': first_name,
            'last_name': last_name,
            'site_url': shop_config.site and shop_config.site.domain or 'localhost',
            'login_url': settings.LOGIN_URL,
            'codice_sconto': sconto
        }

        subject = ugettext("Welcome to %(shop_name)s")
        send_store_mail(subject, c, 'registration/welcome.txt', [contact.email],
                        format_subject=True, sender=registration_sender)
        if settings.ENABLE_SCONTO_BENVENUTO:
            send_store_mail(subject, c, 'registration/buonosconto_benvenuto.txt', [contact.email],
                            format_subject=True, sender=registration_sender)