from django.utils.translation import ugettext as _
from livesettings import config_value
from product.models import Discount
from satchmo_store.mail import NoRecipientsException, send_store_mail, send_store_mail_template_decorator
from satchmo_store.shop.signals import order_confirmation_sender, order_notice_sender, ship_notice_sender
import logging

log = logging.getLogger('contact.notifications')
satchmo_log = logging.getLogger('ex_satchmo')

def order_success_listener(order=None, **kwargs):
    """Listen for order_success signal, and send confirmations"""
    if order:
        send_order_confirmation(order)
        send_order_notice(order)

def notify_on_ship_listener(sender, oldstatus="", newstatus="", order=None, **kwargs):
    """Listen for a transition to 'shipped', and notify customer."""

    if oldstatus != 'Shipped' and newstatus == 'Shipped':
        if order.is_shippable:
            send_ship_notice(order)

@send_store_mail_template_decorator('shop/email/order_complete')
def send_order_confirmation(order, template='', template_html=''):
    """Send an order confirmation mail to the customer.
    """
    from django.template.loader import render_to_string
    from django.conf import settings

    try:
        sale = Discount.objects.by_code(order.discount_code, raises=True)
    except Discount.DoesNotExist:
        sale = None

    c = {'order': order, 'sale' : sale}
    subject = _("Thank you for your order from %(shop_name)s")

    # CASO EXPO. INVO MAIL DA MANDRILL
    if order.source == 'E':
        from ex_satchmo.tools.mandrill import MandrillExpo
        MandrillExpo.send_confirm_order_mail(order=order, sale=sale)
    else:
        if settings.ENABLE_MANDRILL:
            from ex_satchmo.tools.mandrill import MandrillBookrepublic
            MandrillBookrepublic.send_confirm_order_mail(order=order, sale=sale)
        elif settings.ENABLE_ECIRCLE:
            from ex_satchmo.tools.ecircle import Ecircle

            html_dettaglio_ordine = render_to_string('ecircle/order_detail.html', c)
            satchmo_log.info(u"[Order #%s] Ecircle. Invio mail conferma ordine a %s.", order.pk, order.contact.email)
            try:
                ec = Ecircle()
                ec.send_transactional_mail(settings.ECIRCLE_GROUP_ID['bookrepublic'], order.contact.email,
                                           order.contact.first_name, order.contact.last_name,
                                           settings.ECIRCLE_MESSAGE_ID['conferma_ordine'],
                                           ['id_ordine', 'html_dettaglio_ordine'], [order.pk, html_dettaglio_ordine])
                ec.logout()
            except Exception, e:
                satchmo_log.error(u"[Order #%s] Ecircle. Errore in invio mail conferma ordine a %s. Errore %s",
                                  order.pk, order.contact.email, str(e))
                satchmo_log.info(u"[Order #%s] Ecircle. Fallback sulla mail di ordine normale.", order.pk)
                send_store_mail(subject, c, template, [order.contact.email],
                                template_html=template_html, format_subject=True,
                                sender=order_confirmation_sender)
            else:
                satchmo_log.info(u"[Order #%s] Ecircle. Mail conferma ordine a %s inviata correttamente.", order.pk,
                                 order.contact.email)
        else:
            send_store_mail(subject, c, template, [order.contact.email],
                            template_html=template_html, format_subject=True,
                            sender=order_confirmation_sender)

@send_store_mail_template_decorator('shop/email/order_placed_notice')
def send_order_notice(order, template='', template_html=''):
    """Send an order confirmation mail to the owner.
    """

    if config_value("PAYMENT", "ORDER_EMAIL_OWNER"):
        try:
            sale = Discount.objects.by_code(order.discount_code, raises=True)
        except Discount.DoesNotExist:
            sale = None

        c = {'order': order, 'sale' : sale}
        subject = _("New order on %(shop_name)s")

        eddresses = []
        more = config_value("PAYMENT", "ORDER_EMAIL_EXTRA")
        if more:
            eddresses = set([m.strip() for m in more.split(',')])
            eddresses = [e for e in eddresses if e]

        try:
            send_store_mail(subject, c, template, eddresses,
                            template_html=template_html, format_subject=True,
                            send_to_store=True, sender=order_notice_sender)
        except NoRecipientsException:
            log.warn("No shop owner email specified, skipping owner_email")
            return

# TODO add html email template
def send_ship_notice(order, template='shop/email/order_shipped.txt', template_html=''):
    """Send an order shipped mail to the customer.
    """

    log.debug('ship notice on %s', order)

    c = {'order': order}
    subject = _("Your order from %(shop_name)s has shipped")

    send_store_mail(subject, c, template, [order.contact.email],
                    format_subject=True, template_html=template_html,
                    sender=ship_notice_sender)
