from django.conf import settings
from django.views.generic.simple import direct_to_template
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from satchmo_store.invitation.models import InvitationKey
from satchmo_store.invitation.forms import InvitationKeyForm

is_key_valid = InvitationKey.objects.is_key_valid
remaining_invitations_for_user = InvitationKey.objects.remaining_invitations_for_user

def invited(request, invitation_key=None, extra_context=None):
    if getattr(settings, 'INVITE_MODE', False):
        if invitation_key and is_key_valid(invitation_key):
            template_name = 'invitation/invited.html'
        else:
            template_name = 'invitation/wrong_invitation_key.html'
        extra_context = extra_context is not None and extra_context.copy() or {}
        extra_context.update({'invitation_key': invitation_key})
        return direct_to_template(request, template_name, extra_context)
    else:
        return HttpResponseRedirect(reverse('registration_register'))


def invite(request, success_url=None,
            form_class=InvitationKeyForm,
            template_name='invitation/invitation_form.html',
            extra_context=None):
    extra_context = extra_context is not None and extra_context.copy() or {}
    remaining_invitations = remaining_invitations_for_user(request.user)
    invite_sent = InvitationKey.objects.invite_sent_by_user(request.user)
    if request.method == 'POST':
        form = form_class(data=request.POST, files=request.FILES)
        #if remaining_invitations > 0 and form.is_valid():
        if form.is_valid():
            invitation = InvitationKey.objects.create_invitation(request.user, form.cleaned_data['email_1'])
            invitation.send_to(form.cleaned_data["email_1"])

            if form.cleaned_data['email_2']:
                invitation = InvitationKey.objects.create_invitation(request.user, form.cleaned_data['email_2'])
                invitation.send_to(form.cleaned_data["email_2"])
            if form.cleaned_data['email_3']:
                invitation = InvitationKey.objects.create_invitation(request.user, form.cleaned_data['email_3'])
                invitation.send_to(form.cleaned_data["email_3"])

            # success_url needs to be dynamically generated here; setting a
            # a default value using reverse() will cause circular-import
            # problems with the default URLConf for this application, which
            # imports this file.
            return HttpResponseRedirect(success_url or reverse('invitation_complete'))
    else:
        form = form_class()
    extra_context.update({
            'form': form,
            'remaining_invitations': remaining_invitations,
            'invite_sent': invite_sent
        })
    return direct_to_template(request, template_name, extra_context)
invite = login_required(invite)
