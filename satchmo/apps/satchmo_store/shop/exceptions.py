from django.utils.translation import ugettext_lazy as _

class CartAddProhibited(Exception):
    """Raised when a `signals.satchmo_cart_add_verify` listener vetoes adding an item to the cart.

    Params:
    - product: item which was being added
    - message: veto message
    """

    def __init__(self, product, code, message):
        self.product, self._code, self._message = product, code, message

    def _get_message(self):
        return self._message
    message = property(_get_message)

    def _get_code(self):
        return self._code
    code = property(_get_code)


class OutOfStockError(CartAddProhibited):

    def __init__(self, product, have, need):
        if have == 0:
            msg = _("'%s' is out of stock.") % product.translated_name()
        else:
            msg = _("Only %(amount)i of '%(product)s' in stock.") % {
                'amount': have,
                'product': product.translated_name()
                }

        CartAddProhibited.__init__(self, product, 'OUT_OF_STOCK', msg)
        self.have = have
        self.need = need


class CheckoutException(Exception):
    def __init__(self, code, message):
        self._code, self._message = code, message

    def _get_message(self):
        return self._message
    message = property(_get_message)

    def _get_code(self):
        return self._code
    code = property(_get_code)
