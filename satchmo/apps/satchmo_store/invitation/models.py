import os
import random
import datetime
import logging
import string
from django.core.urlresolvers import reverse
from django.db import models
from django.conf import settings
from django.utils.http import int_to_base36
from django.utils.hashcompat import sha_constructor
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.core.mail import send_mail, EmailMessage
from django.template.loader import render_to_string
from django.contrib.sites.models import Site

from registration.models import SHA1_RE
from product.models import Discount

log = logging.getLogger('satchmo_store.invitation')

class InvitationKeyManager(models.Manager):
    def get_key(self, invitation_key):
        """
        Return InvitationKey, or None if it doesn't (or shouldn't) exist.
        """
        # Don't bother hitting database if invitation_key doesn't match pattern.
        if not SHA1_RE.search(invitation_key):
            return None

        try:
            key = self.get(key=invitation_key)
        except self.model.DoesNotExist:
            return None

        return key

    def is_key_valid(self, invitation_key):
        """
        Check if an ``InvitationKey`` is valid or not, returning a boolean,
        ``True`` if the key is valid.
        """
        invitation_key = self.get_key(invitation_key)
        return invitation_key and invitation_key.is_usable()

    def create_invitation(self, user, email):
        """
        Create an ``InvitationKey`` and returns it.

        The key for the ``InvitationKey`` will be a SHA1 hash, generated
        from a combination of the ``User``'s username and a random salt.
        """
        salt = sha_constructor(str(random.random())).hexdigest()[:5]
        key = sha_constructor("%s%s%s" % (datetime.datetime.now(), salt, user.username)).hexdigest()
        return self.create(from_user=user, email_invited=email, key=key)

    def remaining_invitations_for_user(self, user):
        """
        Return the number of remaining invitations for a given ``User``.
        """
        invitation_user, created = InvitationUser.objects.get_or_create(
            inviter=user,
            defaults={'invitations_remaining': settings.INVITATIONS_PER_USER})
        return invitation_user.invitations_remaining

    def invite_sent_by_user(self, user):
        """
        Return the invite sent by a given ``User``.
        """
        return user.invitations_sent.all()

    def delete_expired_keys(self):
        for key in self.all():
            if key.key_expired():
                key.delete()


class InvitationKey(models.Model):
    key = models.CharField(_('invitation key'), max_length=40)
    date_invited = models.DateTimeField(_('date invited'),
                                        default=datetime.datetime.now)
    email_invited = models.EmailField()
    from_user = models.ForeignKey(User,
                                  related_name='invitations_sent')
    registrant = models.ForeignKey(User, null=True, blank=True,
                                  related_name='invitations_used')
    payment_order_made = models.BooleanField(default=False)
    objects = InvitationKeyManager()

    def __unicode__(self):
        return u"Invitation from %s on %s" % (self.from_user.username, self.date_invited)

    def is_usable(self):
        """
        Return whether this key is still valid for registering a new user.
        """
        return self.registrant is None and not self.key_expired()

    def key_expired(self):
        """
        Determine whether this ``InvitationKey`` has expired, returning
        a boolean -- ``True`` if the key has expired.

        The date the key has been created is incremented by the number of days
        specified in the setting ``ACCOUNT_INVITATION_DAYS`` (which should be
        the number of days after invite during which a user is allowed to
        create their account); if the result is less than or equal to the
        current date, the key has expired and this method returns ``True``.

        """
        expiration_date = datetime.timedelta(days=settings.ACCOUNT_INVITATION_DAYS)
        return self.date_invited + expiration_date <= datetime.datetime.now()
    key_expired.boolean = True

    def mark_used(self, registrant):
        """
        Note that this key has been used to register a new user.
        """
        self.registrant = registrant
        self.save()

    def order_made(self):
        """
        Note that the invited User did a payment order.
        """
        self.payment_order_made = True
        self.save()

    def send_email_wo_ecircle(self, email):
        current_site = Site.objects.get_current()
        from_user_contact = self.from_user.contact_set.all()[0]
        subject = render_to_string('invitation/invitation_email_subject.txt',
                                   {'site': current_site,
                                    'invitation_key': self,
                                    'full_name':from_user_contact.full_name})
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())

        url_attivazione = "http://%s%s?invitation_key=%s" % (
            current_site.domain, reverse('registration_register'), self.key)

        message = render_to_string('invitation/invitation_email.txt',
                                   {'invitation_key': self,
                                    'expiration_days': settings.ACCOUNT_INVITATION_DAYS,
                                    'site': current_site,
                                    'full_name': from_user_contact.full_name,
                                    'url_attivazione':url_attivazione})

        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email])

    def send_to(self, email):
        """
        Send an invitation email to ``email``.
        """
        current_site = Site.objects.get_current()

        log.info(u"[Invitation] Ecircle. Invio mail amico invitato a %s.", email)
        url_attivazione = "http://%s%s?invitation_key=%s" % (
            current_site.domain, reverse('registration_register'), self.key)

        if settings.ENABLE_MANDRILL:
            from ex_satchmo.tools.mandrill import MandrillBookrepublic
            MandrillBookrepublic.send_friend_invited_mail(email_amico=email,
                                                          nome_di_chi_invita=self.from_user.contact_set.all()[0].full_name,
                                                          link_iscrizione=url_attivazione)
        elif settings.ENABLE_ECIRCLE:
            from ex_satchmo.tools.ecircle import Ecircle
            try:
                ec = Ecircle()
                ec.send_transactional_mail(settings.ECIRCLE_GROUP_ID['bookrepublic'], email,
                                           '', '',
                                           settings.ECIRCLE_MESSAGE_ID['mail_amico_invitato'],
                                           ['nome_di_chi_invita', 'link_iscrizione'],
                                           [self.from_user.contact_set.all()[0].full_name,
                                            url_attivazione])
                ec.logout()
            except Exception, e:
                log.error(u"[Invitation] Ecircle. Errore in invio mail amico invitato a %s. Errore %s",
                          email, str(e))
                log.info(u"[Invitation] Ecircle. Fallback sulla mail amico invitato normale.")
                self.send_email_wo_ecircle(email)
            else:
                log.info(u"[Invitation] Ecircle. Mail conferma amico invitato a %s inviata correttamente.", email)
        else:
            self.send_email_wo_ecircle(email)

    def send_email_buono_sconto_wo_ecircle(self, sconto, from_user_contact):
        body = render_to_string('invitation/buonosconto_invito_amico.txt', {'sconto': sconto,
                                                                            'first_name':from_user_contact.first_name,})

        msg = EmailMessage(
            subject='Buone notizie per te da parte di Bookrepublic',
            body=body,
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[from_user_contact.email],
        )
        msg.send()
        log.info("[Invitation] Buonosconto creato e inviato correttamente per il contatto %s con relativo utente %s",
                 from_user_contact.full_name, self.from_user.username)

    def check_and_send_discount_code(self):
        """
        Check how many users did a payment order and eventually send to 'from user' a discount code.
        """
        from ex_satchmo.tools.ecircle import Ecircle

        # conto quante persone invitate hanno fatto degli ordini
        try:
            from_user_contact = self.from_user.contact_set.all()[0]
        except Exception:
            from_user_contact = self.from_user
        registrant_contact = self.registrant.contact_set.all()[0]

        inviti = InvitationKey.objects.filter(from_user=self.from_user, payment_order_made=True).count()
        log.info("[Invitation] Invitante %s. Numero di amici invitati che hanno fatto l'ordine : %s",
                 from_user_contact.email, inviti)

        # conto quanti codice sconto ho gia' generato
        num_sconti = self.from_user.inviter.discount_obtained.all().count()
        log.info("[Invitation] Invitante %s. Numero di buoni sconti generati : %s",
                 from_user_contact.email, num_sconti)

        log.info("[Invitation] Invitante %s. Controllo raggiungimento soglia. %s mod %s = %s",
                 from_user_contact.email, inviti, settings.INVITE_NUM_ORDER_REGALO_BUONOSCONTO, inviti % settings.INVITE_NUM_ORDER_REGALO_BUONOSCONTO)

        if inviti % settings.INVITE_NUM_ORDER_REGALO_BUONOSCONTO == 0:
            log.info("[Invitation] Creo un buono sconto 'amico invitato' per il contatto %s con relativo utente %s", from_user_contact.email,
                     self.from_user.username)
            d = Discount()
            d.site = Site.objects.get_current()
            d.description = 'Sconto Invito Amico - %s' % from_user_contact.email

            # Genero un codice alfanumerico random di sicurezza
            chars = string.letters + string.digits
            codice_sicurezza = ''.join([random.choice(chars) for i in range(14)])
            # il codice del buonosconto puo essere di max 20 caratteri.
            # Avendone 4 per la scritta benv, 2 per i trattini e 4 per il codice
            # di sicurezza fermo lo username a max 10 caratteri
            d.code = 'AMICO-%s' % codice_sicurezza.upper()

            d.active = True
            d.amount = settings.INVITE_BUONOSCONTO_AMOUNT
            d.allowedUses = 1
            d.minOrder = settings.INVITE_BUONOSCONTO_MIN_ORDER
            d.startDate = datetime.date.today()
            d.endDate = datetime.date.today() + datetime.timedelta(days=settings.INVITE_BUONOSCONTO_EXPIRE_IN_DAYS)
            d.allValid = True
            d.save()

            # registro il nuovo buonsoconto collegandolo all'utente invitante
            self.from_user.inviter.discount_obtained.add(d)

            log.info("[Invitation] Buonosconto creato per l'utente %s", self.from_user.username)

            if settings.ENABLE_MANDRILL:
                from ex_satchmo.tools.mandrill import MandrillBookrepublic
                MandrillBookrepublic.send_discount_friends_invited(from_user_contact=from_user_contact,
                                                                   discount=d)
            elif settings.ENABLE_ECIRCLE:
                log.info(u"[Invitation] Ecircle. Invio mail codice sconto per amico invitato a %s.", from_user_contact.email)
                try:
                    ec = Ecircle()
                    ec.send_transactional_mail(settings.ECIRCLE_GROUP_ID['bookrepublic'], from_user_contact.email,
                                               from_user_contact.first_name, from_user_contact.last_name,
                                               settings.ECIRCLE_MESSAGE_ID['buono_invito_amico'],
                                               ['codice_sconto', 'data_scadenza'],
                                               [d.code, d.endDate.strftime("%d/%m/%Y")])
                    ec.logout()
                except Exception, e:
                    log.error(u"[Invitation] Ecircle. Errore in invio mail codice sconto per amico invitato a %s. Errore %s",
                              from_user_contact.email, str(e))
                    log.info(u"[Invitation] Ecircle. Fallback sulla mail codice sconto per amico invitato normale.")
                    self.send_email_buono_sconto_wo_ecircle(d, from_user_contact.email)
                else:
                    log.info(u"[Invitation] Ecircle. Mail conferma codice sconto per amico invitato a %s inviata correttamente.", from_user_contact.email)
            else:
                self.send_email_buono_sconto_wo_ecircle(d, from_user_contact.email)
        else:
            log.info("[Invitation] Invitante %s. Non creo nessun buonosconto perche' gli amici invitati non hanno raggiunto la quota di %s acquisti",
                     from_user_contact.email, settings.INVITE_NUM_ORDER_REGALO_BUONOSCONTO)


class InvitationUser(models.Model):
    inviter = models.OneToOneField(User, related_name='inviter')
    invitations_remaining = models.IntegerField()
    discount_obtained = models.ManyToManyField(Discount, blank=True, null=True)

    def __unicode__(self):
        return u"InvitationUser for %s" % self.inviter.username


def user_post_save(sender, instance, created, **kwargs):
    """Create InvitationUser for user when User is created."""
    if created:
        invitation_user = InvitationUser()
        invitation_user.inviter = instance
        invitation_user.invitations_remaining = settings.INVITATIONS_PER_USER
        invitation_user.save()

models.signals.post_save.connect(user_post_save, sender=User)

def invitation_key_post_save(sender, instance, created, **kwargs):
    """Decrement invitations_remaining when InvitationKey is created."""
    if created:
        invitation_user = InvitationUser.objects.get(inviter=instance.from_user)
        remaining = invitation_user.invitations_remaining
        invitation_user.invitations_remaining = remaining-1
        invitation_user.save()

models.signals.post_save.connect(invitation_key_post_save, sender=InvitationKey)

