from decimal import Decimal
from django.conf.urls.defaults import patterns
from satchmo_store.shop.satchmo_settings import get_satchmo_setting
from satchmo_store.shop.models import OrderStatus

ssl = get_satchmo_setting('SSL', default_value=False)

def _confirm_free_order(request, *args, **kwargs):
    """
    fix per far gestire a paypal gli ordini con totale uguale a zero.
    Questa funzione e' un wrapper alla confirm_free_order e, dopo averla
    eseguita, verifica che l'ordine non sia gia' completo; se questo e' il caso
    (e puo' succedere solo se l'ordine era di importo nullo), chiama la
    order_success che a sua volta invoca il segnale order_success.

    La cosa importante e' che venga generato il segnale order_success, perche'
    da questo dipendano molti comportamenti diversi (compreso il completamento
    delle transazioni verso i fornitori).

    Il bug originale consisteva nel non pensare che un ordine potesse risultare
    completo (order.paid_in_full==True) anche senza un pagamento reale, in
    questo caso la confirm_free_order ritorna un HttpResponseRedirect verso la
    pagina con il feedback di "ordine completato con successo" senza quindi
    passare per paypal e senza avviare il meccanismo dell'IPN (nella view ipn
    infatti il modulo paypal crea un PaymentRecorder che a sua volta chiama la
    order_success).
    """
    from payment.views.confirm import confirm_free_order
    from satchmo_store.shop.models import Order
    r = confirm_free_order(request, *args, **kwargs)
    order = Order.objects.from_request(request)
    #payments = order.payments.all()
    #if order.paid_in_full and len(payments) == 1 and payments[0].transaction_id == 'LINKED':
    if order.paid_in_full and order.total == Decimal('0'):
        # XXX il corpo di questa funzione e' copiato da nu pezzo della
        # PaymentRecorder.cleanup
        def _latest_status(order):
            try:
                curr_status = order.orderstatus_set.latest()
                return curr_status.status
            except OrderStatus.DoesNotExist:
                return ''

        if _latest_status(order) in ('', 'New'):
            order.order_success()
            # order_success listeners or product methods could have modified the status. reload it.
            if _latest_status(order) == '':
                order.add_status('New')
    return r

urlpatterns = patterns('',
     (r'^$', 'payment.modules.paypal.views.pay_ship_info', {'SSL': ssl}, 'PAYPAL_satchmo_checkout-step2'),
     (r'^confirm/$', 'payment.modules.paypal.views.confirm_info', {'SSL': ssl}, 'PAYPAL_satchmo_checkout-step3'),
     (r'^success/$', 'payment.modules.paypal.views.success', {'SSL': ssl}, 'PAYPAL_satchmo_checkout-success'),
     (r'^ipn/$', 'payment.modules.paypal.views.ipn', {'SSL': ssl}, 'PAYPAL_satchmo_checkout-ipn'),
     #(r'^confirmorder/$', 'payment.views.confirm.confirm_free_order',
     (r'^confirmorder/$', _confirm_free_order,
         {'SSL' : ssl, 'key' : 'PAYPAL'}, 'PAYPAL_satchmo_checkout_free-confirm')
)
