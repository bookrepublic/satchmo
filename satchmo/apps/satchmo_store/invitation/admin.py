from django.contrib import admin
from satchmo_store.invitation.models import InvitationKey, InvitationUser

class InvitationKeyAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'from_user', 'date_invited', 'email_invited', 'registrant', 'payment_order_made')
    search_fields = ('from_user__email', 'registrant__email', 'email_invited')
    readonly_fields = ('from_user', 'registrant')

class InvitationUserAdmin(admin.ModelAdmin):
    list_display = ('inviter', 'invitations_remaining')
    readonly_fields = ('inviter', 'discount_obtained')

admin.site.register(InvitationKey, InvitationKeyAdmin)
admin.site.register(InvitationUser, InvitationUserAdmin)
