# -*- coding: UTF-8 -*-
from django import forms
from satchmo_store.contact.models import Contact
from satchmo_store.invitation.models import InvitationKey


class InvitationKeyForm(forms.Form):
    email_1 = forms.EmailField()
    email_2 = forms.EmailField(required=False)
    email_3 = forms.EmailField(required=False)

    def common_clean_email(self, nome_campo):
        email = self.cleaned_data.get(nome_campo, None)
        i = InvitationKey.objects.filter(email_invited=email).count()
        if i > 0:
            raise forms.ValidationError(u"Questa email ha già ricevuto un altro invito")
        i = Contact.objects.filter(email__iexact=email).count()
        if i > 0:
            raise forms.ValidationError(u"Questa email risulta già iscritta a Bookrepublic")
        return email

    def clean_email_1(self):
        return self.common_clean_email('email_1')

    def clean_email_2(self):
        return self.common_clean_email('email_2')

    def clean_email_3(self):
        return self.common_clean_email('email_3')